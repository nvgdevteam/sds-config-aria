#!/usr/bin/bash

set -x
echo `pwd` 1>&2

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
yesterday=$(date -d '-1 day' '+%Y-%m-%d')
max_mag="6.5"

${DIR}/query_pdl.py ${yesterday} ${max_mag}
