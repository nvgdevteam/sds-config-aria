#!/usr/bin/env python

'''
Submits a standard create_aoi job via a rest call
'''

import json
import argparse
import requests

#MOZART_URL = 'https://jobs.grfn.hysds.net'
MOZART_URL= 'https://100.64.134.67'


def submit_aoi(release=None, aoi_params=None, aoi_name=None):
    '''
    submits a create_aoi job to mozart
    '''
    # submit mozart job
    job_submit_url = '%s/mozart/api/v0.1/job/submit' % MOZART_URL
    params = {
        'queue': 'factotum-create_aoi-queue',
        'priority': '5',
        'job_name': 'job_%s-%s-%s' % ('create_aoi', aoi_name, release),
        'tags': '["%s"]' % aoi_name,
        'type': 'job-lw-tosca-AOI_create:%s' % release,
        'params': aoi_params,
        'enable_dedup': False

    }
    print('submitting jobs with params:')
    print(json.dumps(params, sort_keys=True,indent=4, separators=(',', ': ')))
    r = requests.post(job_submit_url, params=params)
    if r.status_code != 200:
        r.raise_for_status()
    result = r.json()
    if 'result' in result.keys() and 'success' in result.keys():
        if result['success'] == True:
            job_id = result['result']
            print 'submitted create_aoi:%s job: %s job_id: %s' % (release, job_id)
        else:
            print 'job not submitted successfully: %s' % result
            raise Exception('job not submitted successfully: %s' % result)
    else:
        raise Exception('job not submitted successfully: %s' % result)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="submits a create_aoi job")
    parser.add_argument('-r', '--release', help='create_aoi job version to run (ex: master, release-YYYYMMDD)', dest='release', required=True)
    parser.add_argument('-p', '--params', help='Input params dict', dest='params', required=True)
    parser.add_argument('-n', '--aoiname', help='AOI name for job title', dest='name', required=True)
    args = parser.parse_args()
    submit_aoi(release=args.release, aoi_params=args.params, aoi_name=args.name)
