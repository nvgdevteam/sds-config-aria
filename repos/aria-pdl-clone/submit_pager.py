#!/usr/bin/env python

'''
Submits a standard PAGER job via a REST call, without HySDS reqs
'''

import os
import re
import json
import argparse
import requests
import celeryconfig

def main(minmag, starttime, alertlevel, release, polygon=False):
    '''
    submits a job to mozart to start pager job
    '''
    # submit mozart job
    job_submit_url = os.path.join(celeryconfig.MOZART_URL, 'api/v0.1/job/submit')
    job_params = '{"minmag": "%s", "starttime": "%s", "alertlevel": "%s"}' % (minmag, starttime, alertlevel)
    if polygon != False:
       job_params = '{"minmag": "%s", "starttime": "%s", "alertlevel": "%s", "geojson_polygon": "%s"}' % (minmag, starttime, alertlevel, polygon)
    params = {
        'queue': 'factotum-job_worker-small',
        'priority': '5',
        'tags': '["pager_submission"]',
        'type': 'job-query_pager:%s' % release,
        'params': job_params,
        'enable_dedup': False

    }
    print('submitting jobs with params: %s' %  params)
    r = requests.post(job_submit_url, params=params, verify=False)
    if r.status_code != 200:
        r.raise_for_status()
    result = r.json()
    if 'result' in result.keys() and 'success' in result.keys():
        if result['success'] == True:
            job_id = result['result']
            print 'submitted PAGER job version: %s job_id: %s' % (release, job_id)
        else:
            print 'job not submitted successfully: %s' % result
            raise Exception('job not submitted successfully: %s' % result)
    else:
        raise Exception('job not submitted successfully: %s' % result)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-m', '--minmag', help='Minimum magnitude for query', dest='minmag', required=True)
    parser.add_argument('-t', '--starttime', help='Start time for query', dest='starttime', required=True)
    parser.add_argument('-a', '--alertlevel', help='Minimum alert level threshold (green, yellow, orange or red)', dest='alertlevel', required=True)
    parser.add_argument('-r', '--release', help='job  version to run (ex: master, release-20170914)', dest='release', required=True)
    parser.add_argument('-p', '--polygon', help='geojson polygon filter', dest='polygon', required=False, default=False)
    args = parser.parse_args()
    main(args.minmag, args.starttime, args.alertlevel, args.release, polygon=args.polygon)
