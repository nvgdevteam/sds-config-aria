#!/usr/bin/env python
'''
Query for PDL events, check them against redis if over a minimum magnitude, and submit as create_aoi if it matches minimum criteria
'''
from __future__ import print_function
import json
import re
import os
import sys
import requests
from requests.auth import HTTPBasicAuth
import datetime
import math

from redis import ConnectionPool, StrictRedis
from hysds_commons.net_utils import get_container_host_ip
from hysds_commons.job_utils import submit_mozart_job
from hysds.celery import app

#constraints
MINIMUM_MAGNITUDE = 7.0
MAXIMUM_DEPTH = 50
START_TIME = '2001-01-01'
DAYS_BEFORE = 27 #days to create aoi before event
DAYS_AFTER = 27 #days to create aoi after event
RADIUS = 150
PRIORITY = 5

POOL = None # Redis conn

def query(start_time=None, min_mag=None):
    '''
    runs query over PDL
    '''
    #determine vars
    if start_time == None:
        start_time = START_TIME
    if min_mag == None:
        min_mag = MINIMUM_MAGNITUDE
    #q = 'https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=%s&minmagnitude=%s&maxdepth=%s&alertlevel=red&orderby=time' % (start_time, MINIMUM_MAGNITUDE,MAXIMUM_DEPTH)
    q = 'https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=%s&minmagnitude=%s&alertlevel=red&orderby=time' % (start_time, min_mag)
    print(q)
    session = requests.session()
    response = session.get(q)
    #print(response)
    if response.status_code != 200:
        print("Error: %s\n%s" % (response.status_code,response.text))
        raise QueryBadResponseException("Bad status")

    #parse the json for names and urls
    json_data = json.loads(response.text)
    print('found %s results above %s magnitude post %s' % (len(json_data['features']),min_mag, start_time))
    for event in json_data['features']:
        if not validate(event):
            continue
        submit_aoi_job(event)


def validate(event):
    '''
    Validates the event dict returned from the pdl query. Returns True the event should be
    added, false otherwise.
    '''
    mag = float(event['properties']['mag'])
    if mag < MINIMUM_MAGNITUDE:
        return False

    #query redis to see if event has been ingested
    event_id = event['id']
    return True

def shift(lat, lon, bearing, distance):
    R = 6378.1  # Radius of the Earth
    bearing = math.pi * bearing / 180  # convert degrees to radians
    lat1 = math.radians(lat)  # Current lat point converted to radians
    lon1 = math.radians(lon)  # Current long point converted to radians
    lat2 = math.asin(math.sin(lat1) * math.cos(distance / R) +
                     math.cos(lat1) * math.sin(distance / R) * math.cos(bearing))
    lon2 = lon1 + math.atan2(math.sin(bearing) * math.sin(distance / R) * math.cos(lat1),
                             math.cos(distance / R) - math.sin(lat1) * math.sin(lat2))
    lat2 = math.degrees(lat2)
    lon2 = math.degrees(lon2)
    return [lon2, lat2]

def build_coordinates(event):
    lat = float(event['geometry']['coordinates'][1])
    lon = float(event['geometry']['coordinates'][0])
    mag = float(event['properties']['mag'])
    distance = (mag - 5.0) / 2.0 * RADIUS
    l = range(0, 361, 20)
    coordinates = []
    for b in l:
        coords = shift(lat, lon, b, distance)
        coordinates.append(coords)
    return [coordinates]

def parse_product_name(event):
    '''runs a regex over the place to create a product name'''
    estr = event['properties']['place'] #ex: "69km WSW of Kirakira, Solomon Islands"
    regex = re.compile(' of (.*)[,]? (.*)')
    match = re.search(regex, estr)
    if match:
        product_name = '%s %s' % (match.group(1),match.group(2))
    else:
        product_name = estr
    return product_name.replace(',','')

def parse_browse_url(event):
    default_img = "https://earthquake.usgs.gov/earthquakes/images/latesteqs-300for150.gif"
    try:
        url = event['properties']['detail']
        session = requests.session()
        response = session.get(url)
        json_data = json.loads(response.text)
        browse_img = json_data['properties']['products']['shakemap'][0]['contents']['download/tvmap.jpg']['url']
        return browse_img
    except:
        return default_img

def submit_aoi_job(event):
    #build params from event
    #parse product name
    productname = parse_product_name(event)
    eventid = event['id']
    coordinates = build_coordinates(event)
    location = {"coordinates": coordinates, "type": "polygon"}
    event_dt = datetime.datetime.fromtimestamp(float(event['properties']['time']) / 1000)
    event_time = event_dt.strftime("%Y-%m-%dT%H:%M:%S")
    aoi_name = 'earthquake_%s_%s_%s' % (eventid, event_time[:10], productname.replace(' ','_'))
    stime = (event_dt-datetime.timedelta(DAYS_BEFORE,0)).strftime("%Y-%m-%dT%H:%M:%S")
    etime = (event_dt+datetime.timedelta(DAYS_AFTER,0)).strftime("%Y-%m-%dT%H:%M:%S")
    browse_url = parse_browse_url(event)
    job_spec = "job-lw-tosca-AOI_create:release-20170908"
    queue = "factotum-create_aoi-queue"
    priority = PRIORITY

    #Setup input arguments here
    rule = {
        "rule_name": job_spec,
        "queue": queue,
        "priority": priority,
        "kwargs":"{}"
    }
    params = [
        { "name": "project_name",
          "from": "value",
          "value": 'ariamh',
        },
        { "name": "AOI_name",
          "from": "value",
          "value": aoi_name,
        },
        { "name": "AOI_type",
          "from": "value",
          "value": 'pdl_event',
        },
        { "name": "label",
          "from": "value",
          "value": productname,
        },
        { "name": "coordinates",
          "from": "value",
          "value": location,
        },
        { "name": "event_type",
          "from": "value",
          "value": "earthquake",
        },
        { "name": "event_time",
           "from": "value",
          "value": event_time,
        },
        { "name": "start_time",
          "from": "value",
          "value": stime,
        },
        { "name": "end_time",
          "from": "value",
          "value": etime,
        },
        { "name": "img_URL",
          "from": "value",
          "value": browse_url,
        },
        {
          "name": "use_case",
          "from": "value",
          "value": "no_rules"
        },
        {
          "name": "emails",
          "from": "value",
          "value": "",
        }
    ]
    #check for dedup, if clear, submit job
    #if True:
    if not deduplicate(eventid):
        print('submitting aoi to mozart')
        submit_mozart_job({}, rule, hysdsio={"id": "internal-temporary-wiring", "params": params, "job-specification": job_spec})
        print('aoi_name:', aoi_name)
        print('product name:', productname)
        print('eventid:', eventid)
        print('coordinates:', coordinates)
        #print('eventdt:', event_dt)
        print('event time:', event_time)
        #print('starttime:', stime)
        #print('endtime:', etime)
        print('browse_url:', browse_url)

def deduplicate(title):
    '''
    Prevents the re-adding of aoi's for processed results
    @params title - unique id for pdl event
    '''
    key = 'pdl_events'
    global POOL
    if POOL is None:
        REDIS_URL = "redis://%s" % get_container_host_ip()
        POOL = ConnectionPool.from_url(REDIS_URL)
    r = StrictRedis(connection_pool=POOL)
    return r.sadd(key,title) == 0

def parse_args(arg_list):
    ret = {}
    regex = re.compile('(.*?)=(.*)')
    for arg in arg_list:
        match = re.search(regex, arg)
        if match:
            ret[match.group(1)] = match.group(2)
    return ret

if __name__ == '__main__':
    if len(sys.argv) > 2: 
        starttime = sys.argv[1]
        minmag = sys.argv[2]
    else:
        #run yesterday if no args are supplied
        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
        starttime = yesterday.strftime('%Y-%m-%d')
        minmag = 7.0    
    #print('time: %s, min_mag: %s' % (starttime, minmag))
    query(start_time=starttime, min_mag=minmag)
