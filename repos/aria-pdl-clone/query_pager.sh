#!/bin/bash

set -x
echo `pwd` 1>&2

#check input args
if [ -z "$1" ] 
then
    echo "No minimum magnitude specified"
    exit 1
fi
if [ -z "$2" ] 
then
    echo "No start time specified"
    exit 1
fi
if [ -z "$3" ] 
then
    echo "No alert level specified"
    exit 1
fi

polygon=''
if [ -z "$4" ]
then
    echo "polygon is unset"
else
    polygon="--polygon \"${4}\""
    echo "polygon: ${polygon}"
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
${DIR}/query_pager.py --minmag "${1}" --starttime "${2}" --alertlevel "${3}" ${polygon}
