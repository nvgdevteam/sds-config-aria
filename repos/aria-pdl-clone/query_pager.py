#!/usr/bin/env python
'''
Query for PDL events, check them against redis if over a minimum magnitude, and submit as create_aoi if it matches minimum criteria
'''
from __future__ import print_function
import json
import re
import os
import sys
import requests
import argparse
from requests.auth import HTTPBasicAuth
import datetime
import math
import dateutil.parser

from shapely.geometry import Polygon, Point
import redis
import submit_aoi
import submit_slack_notification
#from redis import ConnectionPool, StrictRedis
from hysds_commons.net_utils import get_container_host_ip
from hysds_commons.job_utils import submit_mozart_job
#from hysds.celery import app

#constraints
CREATE_AOI_RELEASE = 'release-20180306' # default release if not specified
REDIS_KEY = 'pdl_events' # key for aoi event on redis
DAYS_BEFORE = 27 # days to create aoi before event
DAYS_AFTER = 27 # days to create aoi after event
RADIUS = 150 # initial radius calculation at mag
PRIORITY = 7 # create_aoi job priority
POOL = None # Redis conn

def main(starttime=None, minmag=None, alertlevel=None, slack_notification=False, polygon=None, test=False):
    '''
    runs query over PDL, submitting create_AOI jobs for new events
    '''
    #validate args
    if minmag != None:
        try:
            minmag = str(float(minmag))
        except:
            raise Exception('Minimum magnitude not in proper decimal format: %s' % minmag)
        if float(minmag) > 20 or float(minmag) < 0:
            raise Exception('Invalid minimum magnitude threshold. Must be >0 & <20. Magnitude: %s' % minmag)
    options = ['green', 'yellow', 'orange', 'red']
    if alertlevel != None:
        alertlevel = alertlevel.lower()
    if alertlevel not in options:
        alertlevel = None
    if starttime != None:
        try:
            starttime = dateutil.parser.parse(starttime).strftime('%Y-%m-%dT%H:%M:%S')
        except:
            raise Exception('Invalid input starttime: %s' % starttime)
        
    #build query
    print('--------------query thresholds--------------')
    q_minmag = ''
    if minmag != None:
        q_minmag = '&minmagnitude=%s' % minmag
        print('minimum magnitude: %s' % minmag)
    q_alertlevel = ''
    if alertlevel != None:
        q_alertlevel = '&alertlevel=%s' % alertlevel
        print('alert level: %s' % alertlevel)
    q_starttime = ''
    if starttime != None:
        q_starttime = '&starttime=%s' % starttime
        print('start time: %s' % starttime)

    #conduct query
    query = 'https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson%s%s%s&orderby=time' % (q_minmag, q_alertlevel, q_starttime)
    print('--------------running query--------------')
    print(query)
    session = requests.session()
    response = session.get(query)
    #print(response)
    if response.status_code != 200:
        print("Error: %s\n%s" % (response.status_code,response.text))
        raise QueryBadResponseException("Bad status")

    #parse the json for names and urls
    json_data = json.loads(response.text)
    print('query returned %s results' % len(json_data['features']))
    submit_aois = [] # list of aois to submit to create_aoi
    submit_ids = []
    events = json_data['features']
    for event in events:
        if not validate(event, polygon): #pass through spatial filter
            continue
        if not ingested(event):
            #event has been ingested
            print('event %s has not been ingested' % event['id'])
            submit_aois.append(event)
            submit_ids.append(event['id'])
        else:
            print('event %s has already been ingested. Will not submit aoi.' % event['id'])
    if test:
        test_json = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test_event.json')
        test_event = json.load(open(test_json))
        submit_aois.append(test_event)
        submit_ids.append(test_event['id'])


    print('%s events need to be submitted: %s' % (len(submit_ids), ','.join(submit_ids)))
    if len(submit_ids) > 0:
        print('--------------submitting events--------------')
    for event in submit_aois:
        submit_aoi_job(event)
        if slack_notification != False:
            print('submitting slack #aria-sds notification')
            submit_slack_notification.slack_notify(event, slack_notification)
    print('--------------finished query--------------')


def ingested(event):
    '''
    Returns True if the event has been ingested, False otherwise..
    '''
    #query redis to see if event has been ingested
    event_id = event['id']
    global POOL
    if POOL is None:
        redis_url = 'redis://%s' % get_container_host_ip()
        POOL = redis.ConnectionPool.from_url(redis_url)
    r = redis.StrictRedis(connection_pool=POOL)
    return r.sismember(REDIS_KEY, event_id)

def add_event(event):
    '''
    adds the event to the redis key list
    '''
    event_id = event['id']
    global POOL
    if POOL is None:
        redis_url = 'redis://%s' % get_container_host_ip()
        POOL = redis.ConnectionPool.from_url(redis_url)
    r = redis.StrictRedis(connection_pool=POOL)
    r.sadd(REDIS_KEY, event_id)

def shift(lat, lon, bearing, distance):
    R = 6378.1  # Radius of the Earth
    bearing = math.pi * bearing / 180  # convert degrees to radians
    lat1 = math.radians(lat)  # Current lat point converted to radians
    lon1 = math.radians(lon)  # Current long point converted to radians
    lat2 = math.asin(math.sin(lat1) * math.cos(distance / R) +
                     math.cos(lat1) * math.sin(distance / R) * math.cos(bearing))
    lon2 = lon1 + math.atan2(math.sin(bearing) * math.sin(distance / R) * math.cos(lat1),
                             math.cos(distance / R) - math.sin(lat1) * math.sin(lat2))
    lat2 = math.degrees(lat2)
    lon2 = math.degrees(lon2)
    return [lon2, lat2]

def build_coordinates(event):
    lat = float(event['geometry']['coordinates'][1])
    lon = float(event['geometry']['coordinates'][0])
    mag = float(event['properties']['mag'])
    distance = (mag - 5.0) / 2.0 * RADIUS
    l = range(0, 361, 20)
    coordinates = []
    for b in l:
        coords = shift(lat, lon, b, distance)
        coordinates.append(coords)
    return [coordinates]

def parse_product_name(event):
    '''runs a regex over the place to create a product name'''
    estr = event['properties']['place'] #ex: "69km WSW of Kirakira, Solomon Islands"
    regex = re.compile(' of (.*)[,]? (.*)')
    match = re.search(regex, estr)
    if match:
        product_name = '%s %s' % (match.group(1),match.group(2))
    else:
        product_name = estr
    return product_name.replace(',','')

def parse_browse_url(event):
    default_img = "https://earthquake.usgs.gov/earthquakes/images/latesteqs-300for150.gif"
    try:
        url = event['properties']['detail']
        session = requests.session()
        response = session.get(url)
        json_data = json.loads(response.text)
        browse_img = json_data['properties']['products']['shakemap'][0]['contents']['download/tvmap.jpg']['url']
        return browse_img
    except:
        return default_img

def validate(event, polygon_string):
    '''runs the event through a spatial filter if necessary, returns true if passes, false otherwise'''
    if polygon_string == None:
        return False
    try:
        polygon_string = polygon_string.replace('"', '').replace("'", '')
        #print("polygon_string: {}".format(polygon_string))
        json_polygon = json.loads(polygon_string)
        print('geojson: {0}'.format(json_polygon))
        polygon = Polygon(json_polygon)
    except:
        raise Exception('unable to parse input polygon string: {0}'.format(polygon_string))
    lat = float(event['geometry']['coordinates'][1])
    lon = float(event['geometry']['coordinates'][0])
    point = Point(lon, lat)
    if is_covered(point, polygon):
        return True
    return False


def is_covered(point,polygon):
    if polygon.intersects(point):
        return True
    return False


def submit_rest_aoi_job(event):
    productname = parse_product_name(event)
    eventid = event['id']
    coordinates = build_coordinates(event)
    location = {"coordinates": coordinates, "type": "polygon"}
    event_dt = datetime.datetime.fromtimestamp(float(event['properties']['time']) / 1000)
    event_time = event_dt.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    aoi_name = 'earthquake_%s_%s_%s' % (eventid, event_time[:10], productname.replace(' ','_'))
    stime = (event_dt-datetime.timedelta(DAYS_BEFORE,0)).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    etime = (event_dt+datetime.timedelta(DAYS_AFTER,0)).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    browse_url = parse_browse_url(event)
    global CREATE_AOI_RELEASE
    job_spec = "job-lw-tosca-AOI_create:{0}".format(CREATE_AOI_RELEASE)
    queue = "factotum-create_aoi-queue"
    priority = PRIORITY

    params = [
        { "name": "project_name",
          "from": "value",
          "value": 'ariamh',
        },
        { "name": "AOI_name",
          "from": "value",
          "value": aoi_name,
        },
        { "name": "AOI_type",
          "from": "value",
          "value": 'pdl_event',
        },
        { "name": "label",
          "from": "value",
          "value": productname,
        },
        { "name": "coordinates",
          "from": "value",
          "value": location,
        },
        { "name": "event_type",
          "from": "value",
          "value": "earthquake",
        },
        { "name": "event_time",
           "from": "value",
          "value": event_time,
        },
        { "name": "start_time",
          "from": "value",
          "value": stime,
        },
        { "name": "end_time",
          "from": "value",
          "value": etime,
        },
        { "name": "img_URL",
          "from": "value",
          "value": browse_url,
        },
        {
          "name": "use_case",
          "from": "value",
          "value": "no_rules"
        },
        {
          "name": "emails",
          "from": "value",
          "value": "",
        }
    ]
    
    #add event to redis
    #add_event(event)
    print('submitting aoi to mozart')
    print('aoi_name:', aoi_name)
    print('product name:', productname)
    print('eventid:', eventid)
    print('coordinates:', coordinates)
    print('event time:', event_time)
    print('browse_url:', browse_url)

    #run rest aoi submission
    submit_aoi.submit_aoi(release=CREATE_AOI_RELEASE, aoi_params=params, aoi_name=aoi_name)

def submit_aoi_job(event):
    #deprecated
    #build params from event
    #parse product name
    productname = parse_product_name(event)
    eventid = event['id']
    coordinates = build_coordinates(event)
    location = {"coordinates": coordinates, "type": "polygon"}
    event_dt = datetime.datetime.fromtimestamp(float(event['properties']['time']) / 1000)
    event_time = event_dt.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    aoi_name = 'earthquake_%s_%s_%s' % (eventid, event_time[:10], productname.replace(' ','_'))
    stime = (event_dt-datetime.timedelta(DAYS_BEFORE,0)).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    etime = (event_dt+datetime.timedelta(DAYS_AFTER,0)).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    browse_url = parse_browse_url(event)
    global CREATE_AOI_RELEASE
    job_spec = "job-lw-tosca-AOI_create:{0}".format(CREATE_AOI_RELEASE)
    queue = "factotum-create_aoi-queue"
    priority = PRIORITY

    #Setup input arguments here
    rule = {
        "rule_name": job_spec,
        "queue": queue,
        "priority": priority,
        "kwargs":"{}"
    }
    params = [
        { "name": "project_name",
          "from": "value",
          "value": 'ariamh',
        },
        { "name": "AOI_name",
          "from": "value",
          "value": aoi_name,
        },
        { "name": "AOI_type",
          "from": "value",
          "value": 'pdl_event',
        },
        { "name": "label",
          "from": "value",
          "value": productname,
        },
        { "name": "coordinates",
          "from": "value",
          "value": location,
        },
        { "name": "event_type",
          "from": "value",
          "value": "earthquake",
        },
        { "name": "event_time",
           "from": "value",
          "value": event_time,
        },
        { "name": "start_time",
          "from": "value",
          "value": stime,
        },
        { "name": "end_time",
          "from": "value",
          "value": etime,
        },
        { "name": "img_URL",
          "from": "value",
          "value": browse_url,
        },
        {
          "name": "use_case",
          "from": "value",
          "value": "no_rules"
        },
        {
          "name": "emails",
          "from": "value",
          "value": "",
        },
        {
          "name": "username",
          "from": "value",
          "value": "pdl"
        },
        {
          "name": "name",
          "from": "value",
          "value": "pdl"
        }
    ]
   
    #add key to redis 
    add_event(event)
    print('submitting aoi to mozart')
    print('aoi_name:', aoi_name)
    print('product name:', productname)
    print('eventid:', eventid)
    print('coordinates:', coordinates)
    #print('eventdt:', event_dt)
    print('event time:', event_time)
    #print('starttime:', stime)
    #print('endtime:', etime)
    print('browse_url:', browse_url)
    submit_mozart_job({}, rule, hysdsio={"id": "internal-temporary-wiring", "params": params, "job-specification": job_spec})

def parser():
    '''
    Construct a parser to parse arguments
    @return argparse parser
    '''
    parse = argparse.ArgumentParser(description="Run PAGER query with given parameters")
    parse.add_argument("-m", "--minmag", required=False, default=None, help="minimum magnitude for query", dest="minmag")
    parse.add_argument("-t", "--starttime", required=False, default=None, help="Start time for query", dest="starttime")
    parse.add_argument("-a", "--alertlevel", required=False, default=None, help="Minimum alert level threshold for query", dest="alertlevel")
    parse.add_argument("-s", "--slack_notification", required=False, default=False, help="Key for slack notification, will notify via slack if provided.", dest="slack_notification")
    parse.add_argument("-p", "--polygon", required=False, default=None, help="Geojson polygon", dest="polygon")
    parse.add_argument("--test", required=False, default=False, action="store_true", help="Run a test submission", dest="test") 
    return parse

if __name__ == '__main__':
    args = parser().parse_args()
    main(minmag=args.minmag, starttime=args.starttime, alertlevel=args.alertlevel, slack_notification=args.slack_notification, polygon=args.polygon, test=args.test)
