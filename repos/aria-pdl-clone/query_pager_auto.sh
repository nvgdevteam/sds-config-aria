#!/bin/bash

set -x
echo `pwd` 1>&2
echo 'Running tiered PAGER queries covering events over the last day'
if date +%Y-%m-%d -d "yesterday"; then
    starttime=$(date +%Y-%m-%d -d "yesterday")
else
    starttime=$(date -v -1d '+%m-%d-%y')
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
${DIR}/query_pager.py --starttime "${starttime}" --alertlevel "red" --slack_notification True
${DIR}/query_pager.py --minmag "6.0" --starttime "${starttime}" --alertlevel "yellow" --slack_notification True
${DIR}/query_pager.py --minmag "7.0" --starttime "${starttime}" --slack_notification True

