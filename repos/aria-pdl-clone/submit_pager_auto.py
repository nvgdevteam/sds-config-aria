#!/usr/bin/env python

'''
Submits a standard PAGER job via a REST call, without HySDS reqs
'''

import json
import argparse
import requests
import celeryconfig


def main(release):
    '''
    submits a job to mozart to start pager job
    '''
    # submit mozart job
    job_submit_url = os.path.join(celeryconfig.MOZART_URL, 'api/v0.1/job/submit')
    params = {
        'queue': 'factotum-job_worker-small',
        'priority': '5',
        'tags': '["pager_auto"]',
        'type': 'job-query_pager_auto:%s' % release,
        'params': '{}',
        'enable_dedup': False
    }
    print('submitting jobs with params: %s' %  params)
    r = requests.post(job_submit_url, params=params, auth=None, verify=False)
    if r.status_code != 200:
        r.raise_for_status()
    result = r.json()
    if 'result' in result.keys() and 'success' in result.keys():
        if result['success'] == True:
            job_id = result['result']
            print 'submitted pager:%s job_id: %s' % (release, job_id)
        else:
            print 'job not submitted successfully: %s' % result
            raise Exception('job not submitted successfully: %s' % result)
    else:
        raise Exception('job not submitted successfully: %s' % result)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-r', '--release', help='job  version to run (ex: master, release-20170914)', dest='release', required=True)
    args = parser.parse_args()
    main(args.release)
