#! /usr/bin/env bash

/home/ops/verdi/ops/aria-pdl-clone/init.sh start

# run from same directory as script
cd `dirname $0`

# run client
java -jar ProductClient.jar --receive --configFile=config.ini

/home/ops/verdi/ops/aria-pdl-clone/init.sh stop
