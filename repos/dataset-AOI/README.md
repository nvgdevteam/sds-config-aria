# dataset-AOI
Area of interest dataset


## Helper scripts to make AOI

In a temporary staging area, stage two files, both following a particular file name convention.

<aoi name>.txt contains a list of coordinates in GEOJSON order.
<aoi name>.png contains a browse image in PNG format.  (This can be downloaded from web; use convert -format png <input> <output> to convert to PNG) 

Example: 

$ cat kumamoto_japan.txt

[ 126.343, 37.71859032558813],
[ 138.604, 38.03078569382294],
[ 139.175, 28.998531814051795],
[ 126.167, 28.613459424004414],
[ 126.343, 37.71859032558813]

Run the helper script *scripts/make_AOI.sh* to make the AOI directory, which will contain the met.json file and the browse image and thumbnail; this should easily be ingested by *scripts/ingest_AOI.sh*

Example:
$ ../scripts/make_AOI.sh kumamoto_japan.txt
$ mv AOI_kumamoto_japan ..
$ cd ..
$ scripts/ingest_AOI.sh ./AOI_kumamoto_japan


The above is a wrapper script for ingest_dataset.py

## Ingest dataset
```
$ ~/verdi/ops/hysds/scripts/ingest_dataset.py ./AOI_socalgas_storage_alisocanyon ~/verdi/etc/datasets.json
```
