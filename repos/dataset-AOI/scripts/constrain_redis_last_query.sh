#!/bin/bash
if [[ $# -lt 2 ]]; then
  echo "Usage: $0 <AOI> <event time> <delta>"
  echo "Example: $0 AOI-visso_italy_eq \"2016-10-26 19:18:08\" \"-1 month\""
  exit 1
fi

#2016-09-25T00:00:00
mydate=$(date -d "$3 $2" +"%Y-%m-%dT%H:%M:%S")

AOI=$(basename $1)

for type in unavco scihub asf;
do
  echo redis-cli set ${AOI}-${type}-last_query $mydate;
done
