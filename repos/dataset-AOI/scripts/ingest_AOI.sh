#!/bin/bash

if [[ $# -lt 1 ]]; then
  echo "Usage: $0 <AOI dir>"
  echo "Example: $0 ./AOI_central_coast"
  exit 1
fi

AOI_DIR=$1 

if [[ ! -d ${AOI_DIR} ]]; then
  echo "Aborting...can't find ${AOI_DIR}"
  exit 1
fi
~/verdi/ops/hysds/scripts/ingest_dataset.py ${AOI_DIR} ~/verdi/etc/datasets.json 2>&1 | tee -a ingest.log
