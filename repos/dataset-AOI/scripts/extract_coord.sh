#!/bin/bash
if [[ $# -lt 1 ]]; then
  echo "Usage: $0 <json file>"
  echo "Example: $0 antelope_valley.json"
  exit 1
fi

JSON=$1
OUTPUT=$(basename $1 .json).txt


# awk '/coordinates/' antelope_valley.json | sed 's/.*coordinates".*:\[//;s:\]}}::;s:,0\]:]:g;s:],:;],\n:g'
if [[ -e ${OUTPUT} ]]; then
  echo "Aborting... ${OUTPUT} already exists."
  exit 1
fi

awk '/coordinates/' $JSON | sed 's/.*coordinates".*:\[//;s:\]}}::;s:,0\]:]:g;s:],:;],\n:g' > $OUTPUT
