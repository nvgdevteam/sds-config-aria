#!/bin/bash

# If you make the following 2 artifacts available, this script will create the AOI directory, to be ingested by a secondary script.
# You need a list of GeoJSON coordinates, which will be inserted into the AOI met file
# You need a browse image in PNG format for the AOI
# They both need to be named <AOI identifier>.txt and <AOI_identifier>.png, respectively.
# Examples: central_coast.txt and central_coast.png

if [[ $# -lt 1 ]]; then
  echo "Usage: $0 <coordinate list>"
  echo "Example: $0 central_valley.txt"
  exit 1
fi

HEADER=$(dirname $0)/met.header
FOOTER=$(dirname $0)/met.footer
AOI=$(basename $1 .txt)

AOI_DIR=AOI_${AOI}
MET=${AOI_DIR}/${AOI_DIR}.met.json
PNG=${AOI}.png
BROWSE=${AOI_DIR}/browse.png
BROWSE_SM=${BROWSE/.png/_small.png}

mkdir ${AOI_DIR}

if [[ -e ${MET} ]]; then
  echo "Aborting...${MET} already exists"
  exit 1
fi

if [[ ! -e ${PNG} ]]; then
  echo "Aborting...no ${PNG} for browse image"
  exit 1
fi

if [[ ! -e ${BROWSE} ]]; then
  if [[ -e ${BROWSE_SM} ]]; then
    rm -f ${BROWSE_SM}
  fi
  cp -piv ${PNG} ${BROWSE}
  convert -resize 250x250 ${BROWSE} ${BROWSE_SM}
fi
 

sed 's:@AOI_NAME@:'AOI_${AOI}':' ${HEADER} > ${MET}
awk '/^\[/ {printf("%8s"," ")} {print $0}' ${AOI}.txt ${FOOTER} >> ${MET}

echo "Checking ${MET}"
ls ${AOI_DIR} | cat -n
echo
echo ${MET}
cat ${MET}

echo "Please double-check the priority.  Default priority is 7."
