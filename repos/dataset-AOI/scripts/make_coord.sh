#!/bin/bash

# Generate GeoJSON coordinates from bounding box

if [[ $# -lt 4 ]]; then
  echo "Usage: $0 <upper left coordinate> <lower right coordinate>"
  echo "Example: $0 56 -163.5 54.5 -160.5"
  exit 1
fi

latMax=$1
latMin=$3
lonMax=$4
lonMin=$2


echo "[ ${lonMax}, ${latMax} ],"
echo "[ ${lonMax}, ${latMin} ],"
echo "[ ${lonMin}, ${latMin} ],"
echo "[ ${lonMin}, ${latMax} ],"
echo "[ ${lonMax}, ${latMax} ]"
