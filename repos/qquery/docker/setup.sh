#!/bin/bash

# Pulls in ARIA repos outside qquery
scihub_url="https://${GIT_OAUTH_TOKEN}@github.jpl.nasa.gov/aria-hysds/scihub.git"
asf_url="https://${GIT_OAUTH_TOKEN}@github.jpl.nasa.gov/aria-hysds/asf-query.git"
unavco_url="https://${GIT_OAUTH_TOKEN}@github.jpl.nasa.gov/aria-hysds/unavco-query.git"
apihub_url="https://${GIT_OAUTH_TOKEN}@github.jpl.nasa.gov/aria-hysds/apihub.git"
git clone $scihub_url "scihub"
git clone $asf_url "asf"
git clone $unavco_url "unavco"
git clone $apihub_url "apihub"
