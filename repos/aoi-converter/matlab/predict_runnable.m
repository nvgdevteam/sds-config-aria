function predict_runnable(file)
    try
        display(['Reading file:',file])
        eqkpredictor(file,'global_lonlat.llh', struct('minnum',5,'dc',0.003), 'plot',1); quit
    catch ME
        display(['Failing with error:',ME.identifier,' ',ME.message])
        exit(-3);
    end


