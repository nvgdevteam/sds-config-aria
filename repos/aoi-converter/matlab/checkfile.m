% function checkfile(file)
function checkfile(file)
if exist(file,'file') == 0
    fprintf('Error in finding the %s\n', file);
    error([file, ' is not found!',' The script has to stop!']);
end