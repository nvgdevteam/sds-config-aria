% function eqinfo=rd_wphaseCMT(wpCMT)
%
% --------------- sample wpCMT format -----------------
% 11/03/11  5:46:23
% 
% Epicenter:  38.321  142.369
% MW 9.0
% 
% USGS/WPHASE CENTROID MOMENT TENSOR
% 11/03/11 05:46:23.00
% Centroid:   38.321  142.969
% Depth  24         No. of sta:256
% Moment Tensor;   Scale 10**22 Nm
%   Mrr= 1.82       Mtt=-0.13
%   Mpp=-1.69       Mrt= 1.34
%   Mrp= 3.17       Mtp=-0.56
%  Principal axes:
%   T  Val=  3.88  Plg=59  Azm=295
%   N     =  0.03       2      201
%   P     = -3.92      30      110
% 
% Best Double Couple:Mo=3.9*10**22
%  NP1:Strike=193 Dip=14 Slip=  81
%  NP2:        22     76        92
% -------------------------------------------------
% liuz               Jan-2013            original
%
function eqinfo=rd_wphaseCMT(wpCMT)
checkfile(wpCMT);
fid=fopen(wpCMT,'r');
a=textscan(fid,'%s','delimiter','\n');
fclose(fid);
a=a{1};
n=length(a);

%
[Mw,m0,str1,dip1,rak1,str2,dip2,rak2]=deal([]);

%
for i=1: n
    c=findstr('Centroid:',a{i});
    if ~isempty(c)
        b=textscan(a{i},'%*s%f%f','delimiter',':');
        clon=b(2); clat=b(1);
    end
    c=findstr('Depth',a{i});
    if ~isempty(c)
        b=textscan(a{i},'%*s%f');
        cdep=b(1);
    end
    c=findstr('Best Double Couple',a{i});
    if ~isempty(c)
        m0=textscan(a{i},'%*s%s','delimiter','=');
        m0=char(m0{1});
        b=textscan(m0,'%f','delimiter','*');
        b=b{1};
        m0=b(1)*10^b(end);
        Mw=2.0/3.0*(log10(m0)-9.1);
    end
    c=findstr('Mw',a{i});
    if ~isempty(c)
        Mw=textscan(a{i},'%*s%f');
    end
    c=findstr('NP1',a{i});
    if ~isempty(c)
        %c1=textscan(a{i},'%*s%s','delimiter',':');
        %c1=char(c1{1});
        c1=textscan(a{i},'%*s%f','delimiter','='); 
        str1=c1{1}(1);
        dip1=c1{1}(2);
        rak1=c1{1}(3);
    end
    c=findstr('NP2',a{i});
    if ~isempty(c)
        c2=textscan(a{i},'%*s%s','delimiter',':');
        c2=char(c2{1});
        c3=textscan(c2,'%f');
        c3=c3{1};
        [str2,dip2,rak2]=deal(c3(1),c3(2),c3(3));
    end
end

%
eqinfo=struct('Mw',Mw,'m0',m0,'str1',str1,'dip1',dip1,'rak1',rak1,...
    'str2',str2,'dip2',dip2,'rak2',rak2,'clon',clon,'clat',clat,...
    'cdep',cdep);
