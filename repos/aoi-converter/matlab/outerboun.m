% function [x1, y1]=outerboun(x,y)
% -----------------------------------
% LIUZ          original     Feb-2013
%
function [x1,y1]=outerboun(x,y)
dt=DelaunayTri(x,y);
k=convexHull(dt);
x1=x(k);
y1=y(k);

