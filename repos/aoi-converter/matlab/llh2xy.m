% function xy=llh2xy(llh, origin)
% llh: Nx3
% xy: Nx2
function xy =llh2xy(llh, origin)
R=6372.795; % km
deg2rad = pi/180.0;
[lon0,lat0]=deal(origin(1),origin(2));
lon0 = lon0*deg2rad;
lat0 = lat0*deg2rad;
rx0 = R*cos(lat0)*cos(lon0);
ry0 = R*cos(lat0)*sin(lon0);
rz0 = R*sin(lat0);
unx = -sin(lat0)*cos(lon0);
uny = -sin(lat0)*sin(lon0);
unz = cos(lat0);
uex = -sin(lon0);
uey = cos(lon0);
uez = 0.0;

N=size(llh, 1);
x = zeros(N, 1);
y = zeros(N, 1);
for i = 1: N
    lon = llh(i,1)*deg2rad;
    lat = llh(i,2)*deg2rad;
    rx = R*cos(lat)*cos(lon);
    ry = R*cos(lat)*sin(lon);
    rz = R*sin(lat);
    dx = rx-rx0;
    dy = ry-ry0;
    dz = rz-rz0;
    x(i) = dx*unx + dy*uny + dz*unz;
    y(i) = dx*uex + dy*uey + dz*uez;
end
xy = [x y];
