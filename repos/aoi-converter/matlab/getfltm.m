% function m=getfltm(L,W, slip, dip, rake, strike, clon, clat, cdep, origin)
function m=getfltm(L, W, slip, dip, rake, strike, clon, clat, cdep, origin)
deg2rad = pi/180.0;
% avoid singular, also sin(2*90*pi/180) < 0
% but sin(2*89.5*pi/180) > 0. So no sign confusion for ds
if dip == 90
    dip = 89.5;
end
% get llh of the lower edge mid-point
rng=1/2*W*cos(dip*deg2rad);
rng=km2deg(rng);
[mlat,mlon]=reckon(clat,clon,rng,strike+90.0);
mdep=cdep+1/2*W*sin(dip*deg2rad);
ss=slip*cos(rake*deg2rad);
ds=slip*sin(rake*deg2rad);
mxy = llh2xy([mlon mlat 0], origin);
m = [L W mdep dip strike mxy ss ds 0];

