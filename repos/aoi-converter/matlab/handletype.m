% function a=handletype(h)
% a=1, figure handle
% a=0, axis handle
% a=[], N/A
function a=handletype(h)
a=[];
if ishandle(h)
    type=get(h, 'type');
    if strmatch(type,'axes')
        a = 0;
    elseif strmatch(type,'figure')
        a = 1;
    end
else
    a=[];
end