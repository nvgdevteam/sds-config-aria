from __future__ import print_function

import os
import os.path
import sys
import xml.etree.ElementTree as ET


def predict(xmlfile,config):
    '''
    Reads the given quakeml.xml file and runs Zhen's predictor
    @param xmlfile - quakeml xml file to harvest
    @return: geolocation coordinates
    '''
    mdir = config["matlab-dir"]
    code = os.path.dirname(__file__)
    tmpfile = os.path.join(code,config["matlab-tmp-input-file"])
    try:
        os.remove(tmpfile)
    except OSError as e:
        pass
    data = xml(xmlfile)
    genFile(data,tmpfile)
    lonlats = run(code,mdir,tmpfile)
    #Bounding box correction
    if len(lonlats) == 2:
        lonlats = [lonlats[0],[lonlats[0][0],lonlats[1][1]],lonlats[1],[lonlats[1][0],lonlats[0][1]],lonlats[0]]
    return lonlats
def chainText(root,tags,ns):
    '''
    Find a chain of tags under the given root tag and return last element's text
    @param root - root element to find from
    @param tags - ordered list of tags
    @ns - namespace map
    @return: text child of the last tag
    '''
    tags.insert(0,"./")
    search = "/".join(tags)
    tags.pop(0)
    try:
        ret = root.findall(search,ns)[0].text.strip()
    except:
        raise InvaldQuakeMLException("Could not find {0} under {1}".format("/".join(tags),root.getroot().tag))
    if ret == "":
        raise InvaldQuakeMLException("{0} under {1} has no text".format("/".join(tags),root.getroot().tag))
    return ret
def xml(xmlfile):
    '''
    Read XML file and emit key parameters
    @param xmlfile - quakexml.xml
    @return: param map
    '''
    root = ET.parse(xmlfile)
    ns = {
        "q":"http://quakeml.org/xmlns/bed/1.2",
        "cat":"http://anss.org/xmlns/catalog/0.1"
    }
    #Get event public ID
    oid = chainText(root,["q:derivedOriginID"],ns)
    
    ret = {}
    #Magnitude
    if chainText(root,["q:magnitude","q:type"],ns) == "Mw":
        raise InvaldQuakeMLException("Invaliid magnitude type: {0}".format(chainText(root,["q:magnitude","q:type"],ns)))
    ret["Mw"] = float(chainText(root,["q:magnitude","q:mag","q:value"],ns))
    #Scalar Moment (Nm)
    ret["Nm"] = float(chainText(root,["q:momentTensor","q:scalarMoment","q:value"],ns))
    #Moment tensor
    for item in ["Mrr","Mpp","Mrp","Mtt","Mrt","Mtp"]:
        ret[item] = float(chainText(root,["q:tensor","q:{0}".format(item),"q:value"],ns))/(10**18)
    #Axis
    for item in ["T","N","P"]:
        az = float(chainText(root,["q:principalAxes","q:{0}".format(item.lower())+"Axis","q:azimuth","q:value"],ns))
        pl = float(chainText(root,["q:principalAxes","q:{0}".format(item.lower())+"Axis","q:plunge","q:value"],ns))
        val = float(chainText(root,["q:principalAxes","q:{0}".format(item.lower())+"Axis","q:length","q:value"],ns)) / 10**18
        ret[item] = {"Val":val,"Plg":pl,"Azm":az}
    for plane in ["1","2"]:
        strike = float(chainText(root,["q:nodalPlane{0}".format(plane),"q:strike","q:value"],ns))
        dip = float(chainText(root,["q:nodalPlane{0}".format(plane),"q:dip","q:value"],ns))
        rake = float(chainText(root,["q:nodalPlane{0}".format(plane),"q:rake","q:value"],ns))
        ret["NP"+plane] = {"Strike":strike,"Dip":dip,"Rake":rake}
    #Origin
    lat = float(chainText(root,["q:origin[@publicID='{0}']".format(oid),"q:latitude","q:value"],ns))
    lon = float(chainText(root,["q:origin[@publicID='{0}']".format(oid),"q:longitude","q:value"],ns))
    depth = float(chainText(root,["q:origin[@publicID='{0}']".format(oid),"q:depth","q:value"],ns))
    ret["origin"] = {"lat":lat,"lon":lon,"depth":depth}
    time = chainText(root,["q:origin[@publicID='{0}']".format(oid),"q:time","q:value"],ns)
    ret["time"] = time
    return ret
def genFile(data,tmp):
    '''
    Generates output file to feed into Matlab
    @param data - data to import
    @param tmp - temporary data
    '''
    vals = {"mw":data["Mw"],"lat":data["origin"]["lat"],"lon":data["origin"]["lon"],"depth":data["origin"]["depth"]/1000,
            "mo":str(data["Nm"]).replace("e+","*10**"),"stk1":data["NP1"]["Strike"],"dip1":data["NP1"]["Dip"],"slip1":data["NP1"]["Rake"],
            "stk2":data["NP2"]["Strike"],"dip2":data["NP2"]["Dip"],"slip2":data["NP1"]["Rake"]}
    tempstr = '''XXXXXXXX

Epicenter:  XXXXX  XXXXX
MW {mw}

USGS/WPHASE CENTROID MOMENT TENSOR
XXXXXXXXXXXX
Centroid:   {lat}  {lon}
Depth  {depth}         No. of sta:XXX
Moment Tensor;   Scale XXXXX
Mrr= XXX       Mtt=XXXX
Mpp=XXXX       Mrt=XXXX
Mrp=XXXX       Mtp=XXXX
Principal axes:
T  Val=  XXXX  Plg=XX  Azm=XXX
N     =  XXXX      XX      XXX
P     =  XXXX      XX      XXX

Best Double Couple:Mo={mo}
NP1:Strike={stk1} Dip={dip1} Slip={slip1}      
NP2:       {stk2}     {dip2}      {slip2}      
        '''.format(**vals)
    with open(tmp,"w") as f:
        print(tempstr,file=f)
def run(codedir,mdir,tmpfile):
    '''
    Run the predictor given the tmpfile
    @param codedir - directory of code
    @param mdir - matlab directory
    @param tmpfile - name of tmpfile to run
    '''
    try:
        os.remove(os.path.join(mdir,"eqregion.txt"))
        os.remove(os.path.join(mdir,"eqsites.list"))
    except OSError as e:
        pass
    cd = os.getcwd()
    #mcmd = "predict_runnable('{0}'); quit;".format(tmpfile)
    #cmd = "cd {0}; {1} -r \"{2}\"".format(codedir,os.path.join(mdir,"bin","matlab"),mcmd)
    cmd = "cd {0}; {1} {2} {3}".format(codedir,"exe/run_predict_runnable.sh",mdir,tmpfile)
    ret = os.system(cmd)
    if ret == 64768:
    #    os.system("cp /home/ops/aoi-converter/matlab/super-temp-1 \"fail/`date`.fail\"")
        raise Exception("Matlab suffered a failure")
    #os.system("cp /home/ops/aoi-converter/matlab/super-temp-1 \"succ/`date`.succ\"")
    return read(os.path.join(codedir,"eqregion.txt"))
def read(tmpfile):
    '''
    Read resulting file
    @param tmpfile - file to read lat lons from
    '''
    lonlats = []
    with open(tmpfile) as f:
        for line in f.readlines():
            lonlats.append([float(item) for item in line.split()])
    lonlats.reverse()
    return lonlats

class InvaldQuakeMLException(Exception):
    pass
