% function a=rdllh(apriori_file)
% input: 
%      apriori_file: a priori coordinate file
%                    (lon, lat, height(m)), where lon, lat in deg
% output:
%      a=struct('sites','lon','lat','h')
%
%
%

function a=rdllh(apf)
checkfile(apf);
fid=fopen(apf,'r');
b=textscan(fid,'%s%f%f%f');
fclose(fid);

a.sites=b{1};
a.lon=b{2};
a.lat=b{3};
a.h=b{4};
