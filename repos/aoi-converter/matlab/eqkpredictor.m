% eqkpredictor(wpCMT, siteposlist, thsld, varargin)
% Aria earthquake coseismic deformation predictor 
% input:
%         siteposlist: e.g., 'global_lonlat.llh'
%         wpCMT - wphase CMT focal mechanism
%         thsld - threshold criteria, if [], default struct('minnum',5,'dc',0.005)
%                 dc - unit, m
% output:
%         'eqsite.list'; 
%         'eqregion.txt'
% call:
%         rd_wphaseCMT.m
%         llh2local.m  
%         disloc3d.m
%         getflt.m
%         disloc3d.mexa64
%
% -------------------------------------------------
% LIUZ              origina            Jan,Feb-2013
%

function eqkpredictor(wpCMT, siteposlist, thsld, varargin)
plt=0;
if length(varargin) > 1
    for v=1:length(varargin)
        if ischar(varargin{v})
            switch lower(varargin{v})
                case 'plot'
                    plt=varargin{v+1};
            end
        end
    end
end

% eq 
wCMT=wpCMT;
eqinfo=rd_wphaseCMT(wCMT);
clon=eqinfo.clon;
clat=eqinfo.clat;
cdep=eqinfo.cdep;
moment=eqinfo.m0; % Nm
%if clon < 0
%    clon = 360.0 + clon;
%end

% site
list=siteposlist; 
allsites=rdllh(list);
sll=[allsites.lon'; allsites.lat'];
sllh=[sll; zeros(1,size(allsites.lon,1))];

% threshold
% default: >= 5 sites with any of predicted E,N,U jump >= 5 mm  
if isempty(thsld)
    thsld=struct('minnum',5,'dc',0.005);
end
minnum=thsld.minnum;
dc=thsld.dc; 

%
mu=4.0*10^10;
deg2rad=pi/180.0;
km2m=1000;
 
% origin
origin=[clon clat]; 
sxy=llh2local(sllh, origin);
sxyz=[sxy; zeros(1,size(sxy,2))];

% get slip (here stress-drop module comes in)
slipflag=0;
if ~slipflag
    L=3; W=3;
    slip = moment/L/W/mu/(km2m^2); 
else
%    [slip, L, W] = getslip(moment, mu, cdep, ctag);
end
    
% get 1st nodal plane
[strike,dip,rak]=deal(eqinfo.str1, eqinfo.dip1, eqinfo.rak1);
m1 = getfltm(L, W, slip, dip, rak, strike, clon, clat, cdep, origin);
% 2nd nodal plane
[strike,dip,rak]=deal(eqinfo.str2, eqinfo.dip2, eqinfo.rak2);
m2 = getfltm(L, W, slip, dip, rak, strike, clon, clat, cdep, origin);
clon1=clon;
% prepare ones for region outline
if clon <0, clon1=clon+360; end
m11 = getfltm(L, W, slip, dip, rak, strike, clon1, clat, cdep, [clon1,clat]);
m22 = getfltm(L, W, slip, dip, rak, strike, clon1, clat, cdep, [clon1,clat]); 
  
% calculate surface displacement
[U1,D1,S1,flag]=disloc3d(m1',sxyz, mu, 0.25);
[U2,D2,S2,flag]=disloc3d(m2',sxyz, mu, 0.25);
J1=find((abs(U1(1,:))>=dc)|(abs(U1(2,:))>=dc)|(abs(U1(3,:))>=dc));
J2=find((abs(U2(1,:))>=dc)|(abs(U2(2,:))>=dc)|(abs(U2(3,:))>=dc));
J=union(J1,J2);
J=unique(J);
dvec=U1(1:3,J)';
x=allsites.lon(J);
y=allsites.lat(J); 
fid=fopen('eqsite.list','w');
if length(J) >=minnum
    for i=1: length(J)
        k=J(i);
        fprintf(fid,'%s %f %f %f\n',allsites.sites{k}, x(i), y(i), allsites.h(k));
    end
else
    msg='stations affected by the eqrthquake, which is less than';
    fprintf(fid,'%d %s the threshold number of %d\n',length(J), msg, minnum);
end
fclose(fid);
%
if isempty(J)
    [x,y,dvec]=deal([]);
    xmin=clon-5;
    xmax=clon+5;
    ymin=clat-5;
    ymax=clat+5;
else
    xmin=min(x);
    xmax=max(x);
    ymin=min(y);
    ymax=max(y);
end

% get the bounding box
glat=-90.0:0.5:90.0;
glat(glat==0)=0.001;
glon=0:0.5:360.0;
[lon1,lat1]=meshgrid(glon,glat);
gll=[lon1(:) lat1(:)];
N=size(gll,1);
[ux,uy,uz]=deal(zeros(N,1));
origin1=[clon1 clat];
gllh=[gll'; zeros(1,size(gll,1))];
gxy=llh2local(gllh,origin1);
gxyz=[gxy; zeros(1,length(gxy))];
[U1,D1,S1,flag]=disloc3d(m11',gxyz, mu, 0.25);
[U2,D2,S2,flag]=disloc3d(m22',gxyz, mu, 0.25);
J1=find((abs(U1(1,:))>=dc)|(abs(U1(2,:))>=dc)|(abs(U1(3,:))>=dc));
J2=find((abs(U2(1,:))>=dc)|(abs(U2(2,:))>=dc)|(abs(U2(3,:))>=dc));
J=union(J1,J2);
J=unique(J);
% output bounding box
if ~isempty(J)
    [x1,y1]=outerboun(gll(J,1),gll(J,2));
else
    [x1,y1]=deal([]);
    xmin1=clon1-5;
    xmax1=clon1+5;
    ymin=clat-5;
    ymax=clat+5;
    x1=[xmin1; xmax1];
    y1=[ymin; ymax];
end
fid=fopen('eqregion.txt','w');
if ~isempty(x1)
    x1(x1>180)=x1(x1>180)-360;
    fprintf(fid,'%f %f\n',[x1,y1]');
else
    fprintf(fid,'no detectable surface deformation \n');
end
fclose(fid);


