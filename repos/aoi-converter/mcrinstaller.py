#!/usr/bin/env python
from __future__ import print_function
import os
import sys
import osaka.main

def install(tmpdir,installdir):
    '''
    Install MCR
    '''
    osaka.main.get("https://aria2-dav.jpl.nasa.gov/installation/MCRInstaller.zip","{0}/MCRInstaller.zip".format(tmpdir))
    os.system("unzip -d {0} {0}/MCRInstaller.zip".format(tmpdir))
    os.system("rm -rf {0}/*".format(installdir))
    os.system("{0}/install -mode silent -agreeToLicense yes -destinationFolder {1}".format(tmpdir,installdir))
    os.system("rm -rf {0}".format(tmpdir))
    lbase = os.path.join(installdir,"v85","sys/os/glnxa64")
    ffrom = os.path.join(lbase,"libgfortran.so.3.0.0")
    fto = os.path.join(lbase,"libgfortran.so.1.0.0")
    os.system("ln -s {0} {1}".format(ffrom,fto))
    fto = os.path.join(lbase,"libgfortran.so.1")
    os.system("ln -s {0} {1}".format(ffrom,fto))
if __name__ == "__main__":
    if len(sys.argv) != 2 or not os.path.isdir(os.path.dirname(sys.argv[1])):
         print("Usage:\n\tmcrinstaller.py <install dir>",file=sys.stderr)
         sys.exit(-1)
    install("/tmp/mcr-install",sys.argv[1])
