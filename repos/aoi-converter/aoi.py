#!/usr/bin/env python
import sys
import json
import os.path
import logging
import datetime
import xml.etree.ElementTree
import traceback
#import hysds.orchestrator
#import hysds.product_ingest

import matlab.predict
from hysds.celery import app
from hysds_commons.job_utils import submit_mozart_job

REQUIRED_FIELDS = ["magnitude","depth","latitude","longitude","eventids","eventtime"]

def parse(args):
    '''
    Processes unknown command line arguments
    @param args - args to parse
    @return map of name to argument
    '''
    ret = {}
    for arg in args:
        if arg.startswith("--"):
            sp = arg.replace("--","",1).split("=")
            if len(sp) >= 2:
                ret[sp[0]] = sp[1]
            else:
                ret[sp[0]] = True
    return ret
def metadata(productname,productdir,coordinates,priority,extras={}):
    '''
    Generates met.json for this area of interest
    @param productname - name of this area of interest
    @param coordinates - polygon coordinates for this area of interest
    '''
    metadata = {
                   "dataset":"area_of_interest",
                   "data_product_name": productname,
                   "location" : {
                       "type": "polygon",
                       "coordinates":[coordinates]
                   },
                   "priority":priority
               }
    metadata.update(extras)
    fn = os.path.join(productdir,productname+".met.json")
    with open(fn,"w") as fp:
        json.dump(metadata,fp)
def coordinates(quakeml,config,lat,lon,radius):
    '''
    Coordinates
    @param quakeml - quakeml XML file name
    @param lat - latitude at center
    @param lon - longitude of center
    @param radius - radius of this area of interest
    @return square bounding box circumscribing circle of given radius from point 
    '''
    try:
        ret = matlab.predict.predict(quakeml,config)
        if len(ret) <= 2:
            raise Exception("too few bounding-box coordinates")
        logging.info("Matlab bounding-box predictions generated: "+str(ret))
        return ret
    except Exception as e:
        logging.warning("Failed to run matlab bounding-box predictor: "+str(e)+" in "+quakeml)
    minlat = lat-radius
    maxlat = lat+radius
    minlon = lon-radius
    maxlon = lon+radius
    logging.info("Using {0} radius around epicenter as bounding box".format(radius))
    return [[maxlon,maxlat],[maxlon,minlat],[minlon,minlat],[minlon,maxlat],[maxlon,maxlat]]
def configuration():
    '''
    Get the configuration for this converter
    @return loaded configuration
    '''
    name = os.path.join(os.path.dirname(os.path.realpath(__file__)),"aoi-converter.json")
    config = {}
    with open(name) as fp:
        logging.info("Reading configuration from: "+name)
        config = json.load(fp)
    #Convert linear configuration arguments into a callable function
    for k,v in config.iteritems():
        try:
            if "type" in v and "slope" in v and "intercept" in v and v["type"] == "linear":
                slope =  v["slope"]
                intercept = v["intercept"] 
                config[k] = (lambda x: slope * x + intercept)
        except TypeError as e:
            if not "is not iterable" in str(e):
                raise e
    return config

def harvest(product):
    '''
    Harvest the given product
    @param product - path to origin product
    @return properties harvested from products.xml
    '''
    properties = {}
    root = xml.etree.ElementTree.parse(product).getroot()
    for elem in root.findall(root.tag[:-7]+"property"):
        attributes = elem.attrib
        try:
            properties[attributes["name"]] = float(attributes["value"])
        except ValueError:
            properties[attributes["name"]] = attributes["value"]
    return properties

def accept(properties,config):
    '''
    Should we accept this product?
    @param properties - properties from this product
    @param config - configuration params for filtering
    @return True if it should be accepted, False otherwise
    '''
    for req in REQUIRED_FIELDS:
        if not req in properties:
            logging.info("Product does not define required field: {0}".format(req))
            return False
    if properties["magnitude"] < config["minimum-magnitude"]:
        logging.info("Product's magnitude of {0} is below threshold of {1}".format(properties["magnitude"],config["minimum-magnitude"]))
        return False
    elif "maximum-depth" in config and properties["depth"] > config["maximum-depth"](properties["magnitude"]):
        logging.info("Product's depth of {0} is above threshold of {1}".format(properties["depth"],config["maximum-depth"](properties["magnitude"])))
        return False
    #TODO: Over land mask
    return True

def radius(config):
    '''
    Get the radius for this reading
    @param config - configuration
    '''
    #TODO: Make this radius based on the magnitude
    return config["radius"]/111

#def ingest(directory):
#    '''
#    Ingest a given directory
#    @param directory - directory of product
#    @param config - configuration to use
#    '''
#    hysds.product_ingest.ingest(os.path.join(os.path.dirname(os.path.realpath(__file__)),"datasets.json"),
#
#                                hysds.celery.app.conf.GRQ_UPDATE_URL,hysds.celery.app.conf.PRODUCT_PROCESSED_QUEUE,directory,None)

if __name__ == "__main__":
    '''
    Main method
    '''
    args = parse(sys.argv)
    log_format = "[%(asctime)s: %(levelname)s/%(funcName)s] %(message)s"
    logging.basicConfig(format=log_format, level=logging.INFO, filename=args["log"])
    #Get product file
    config = configuration()    
    properties = {}
    #Pass on event update
    if "action" in args:
        if args["action"] == "EVENT_UPDATED":
            logging.info("Event update, not passing forward")
            sys.exit(0)
    if "directory" in args:
        product = args["directory"]
        product = os.path.join(product,"product.xml") 
        try:
            properties = harvest(product)
        except IOError as e:
            logging.warning("Failed to harvest product file at: {0}".format(product))
            sys.exit(-5) 
    #Overrides for non-standard products
    for req in REQUIRED_FIELDS:
        for source in ["property-derived-"+req,"preferred-"+req,req]:
            if not req in properties:
                try:
                    properties[req] = float(args[source])
                except KeyError as e:
                    continue
                except ValueError as ve:
                    if "could not convert string to float" in str(ve):
                        properties[req] = args[source]
    #Is this product ok to ingest?
    if "delete" in args and args["delete"]:
        logging.info("Delete of product is unhandled")
        sys.exit(0)
    elif not accept(properties,config):   
        sys.exit(0)
    #Build product
    rad = radius(config)
    #Get event id
    eventid = "Around_({0},{1})".format(properties["latitude"],properties["longitude"])
    if "eventsource" in properties and "eventsourcecode" in properties:
        eventid =  properties["eventsource"] + properties["eventsourcecode"]
    elif "eventids" in properties and len(properties["eventids"]) > 0:
        eventid =  properties["eventids"][0]
    productname = "AOI_Event_Area_{0}".format(eventid)
    productdir = os.path.join("work-dir",productname)
    logging.info("Creating product: {0}".format(productname))
    coordinates = coordinates(os.path.join(args["directory"],"quakeml.xml"),config,properties["latitude"],properties["longitude"],rad)
    if not os.path.isdir(productdir):
        os.makedirs(productdir)
    stime = (datetime.datetime.strptime(properties["eventtime"],"%Y-%m-%dT%H:%M:%S.%fZ")-datetime.timedelta(27,0)).strftime("%Y-%m-%dT%H:%M:%S")
    etime = (datetime.datetime.strptime(properties["eventtime"],"%Y-%m-%dT%H:%M:%S.%fZ")+datetime.timedelta(27,0)).strftime("%Y-%m-%dT%H:%M:%S")
    event_time = properties["eventtime"]

    job_spec = "lw-tosca-AOI_earthquake_response"
    queue = "factotum-jobs-queue"
    priority = 5

    #Setup input arguments here
    rule = {
        "rule_name": job_spec,
        "queue": queue,
        "priority": priority,
        "kwargs":"{}"
    }
    params = [
        { "name": "project_name",
          "from": "value",
          "value": 'ariaml',
        },
        { "name": "AOI_name",
          "from": "value",
          "value": productname,
        },
        { "name": "label",#human readable label
          "from": "value",
          "value": eventid,
        },
        { "name": "coordinates",
          "from": "value",
          "value": coordinates,
        },
        { "name": "event_type",
          "from": "value",
          "value": "earthquake",
        },
        { "name": "event_time",
           "from": "value",
          "value": event_time,
        },
        { "name": "start_time",
          "from": "value",
          "value": stime,
        },
        { "name": "end_time",
          "from": "value",
          "value": etime,
        },
        { "name": "img_URL",
          "from": "value",
          "value": "https://earthquake.usgs.gov/earthquakes/images/latesteqs-300for150.gif",
        },
        { "name": "workflow",
          "from": "value",
          "value": "hysds-io.json.sciflo-s1-ifg",
        },
        { "name": "workflow_version",
          "from": "value",
          "value": "v2.0_002-ops",
        },
        #{ "name": "track_number", #optional field
        #  "from": "value",
        #  "value": "not specified",
        #},
        { "name": "tem_baseline", #temporal baseline for interferogram workflow
          "from": "value",
          "value": "not specified",
        },
        { "name": "use_case",
          "from": "value",
          "value": "event_response",
        },
        { "name": "query",
          "from": "value",
          "value": "",
        },
        { "name": "emails",
          "from": "value",
          "value": "Justin.P.Linick@jpl.nasa.gov"
        }

    ]
    #check for dedup, if clear, submit job
    if not self.deduplicate(eventid):
        submit_mozart_job({}, rule,
            hysdsio={"id": "internal-temporary-wiring",
                    "params": params,
                    "job-specification": job_spec})







