from sdscli.adapters.hysds.fabfile import *


#####################################
# add custom fabric functions below
#####################################

def test():
    """Test fabric function."""

    run('whoami')


def update_aria_packages():
    """Update verdi and factotum with ARIA packages."""

    role, hysds_dir, hostname = resolve_role()

    # update common packages between verdi and factotum
    if role in ('verdi', 'factotum'):
        rm_rf('%s/ops/ariamh' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/ariamh',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        rm_rf('%s/ops/tropmap' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/tropmap',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        rm_rf('%s/ops/apihub' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/apihub',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        rm_rf('%s/ops/scihub' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/scihub',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        rm_rf('%s/ops/asf' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/asf',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        rm_rf('%s/ops/unavco' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/unavco',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        rm_rf('%s/ops/scihub_acquisition_scraper' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/scihub_acquisition_scraper',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")

    # update packages for factotum
    if role == 'factotum':
        rm_rf('%s/ops/qquery' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/qquery',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        rm_rf('~/dataset-AOI')
        rsync_project('~/', '~/mozart/ops/dataset-AOI',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        rm_rf('~/aoi-converter')
        rsync_project('~/', '~/mozart/ops/aoi-converter',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        #run("~/aoi-converter/mcrinstaller.py ~/mcr")
        rm_rf('%s/ops/s1_qc_ingest' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/s1_qc_ingest',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        #rm_rf('%s/ops/aria-pdl-clone' % hysds_dir) #don't delete data, storage and log directories
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/aria-pdl-clone',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")
        rm_rf('%s/ops/aoi-converter' % hysds_dir)
        rsync_project('%s/ops/' % hysds_dir, '~/mozart/ops/aoi-converter',
                      ssh_opts="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no")

    # update ariamh settings
    rm_rf('%s/ops/ariamh/conf/settings.conf' % hysds_dir)
    send_template('ariamh_settings.conf', '%s/ops/ariamh/conf/settings.conf' % hysds_dir)

    # update netrc
    rm_rf('%s/.netrc' % hysds_dir)
    send_template('netrc', '~/.netrc')
