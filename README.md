# SDS configuration from "sds configure"

## Update
```
mv ~/.sds ~/.sds.orig
tar xvf sds-config-aria-azure.tbz2
mv sds-config-aria ~/.sds
cp ~/.sds.orig/config ~/.sds/config
sds update all
fab -f ~/.sds/cluster.py -R factotum,verdi update_aria_packages
sds ship 
sds reset all
```
